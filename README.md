This is the implementation of the following paper:

> Shudan Zhong and Hong Xu. Intelligently recommending key bindings on physical keyboards with demonstrations in Emacs. In Proceedings of the 24th International Conference on Intelligent User Interfaces (IUI), 12–17. 2019. doi:10.1145/3301275.3302272.